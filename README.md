The project is under Development.


The idea is to create a DSL for the entire Google Maps API.
For starters,you initiate a client :
```groovy
def apiKey ="your_api_key"
def client = new GoogleMapsClient(apiKey:apiKey)
```

Now,want to geocode something based on certain parameters ?
```groovy
GeocodingApiResult result = client.geocode{
	address 'some_address_here'
	northeastBound{
		lat -12.222122
		lng  23.11222122
	}
	southwestBound{
		lat  -11.2112222
		lng  22.9876543
	}
	filter{
		route 
		administrativeArea 'TX'
        locality
		postalCode 
		country Country.IN
	}
	language Language.EU
	region Region.IN
}

List<GeographicalEntity> geoEntities = result.entities
```

And for async :

```groovy
Call<GeocodingApiResult> result = client.geocode{
	address 'some_address_here'
	northeastBound{
		lat -12.222122
		lng  23.11222122
	}
	southwestBound{
		lat  -11.2112222
		lng  22.9876543
	}
	filter{
		route 
		administrativeArea 'TX'
        locality
		postalCode 
		country Country.IN
	}
	language Language.EU
	region Region.IN
}

List<GeographicalEntity> entities 
result.callback([
    onSuccess : {List<GeographicalEntity> entity ->
        //Do something.
    },
    onFailure :{
        //handle error
    }
] as PendingResult)


```

Google Directions API

```groovy
TimezoneApiResult result = client.request{
    location{
        lat -12.12
        lng 11.22
    }
    timestamp 121212
    language Language.IN
}
```

Google Roads Api

```groovy
DirectionsApiResult result = client.directions{
    origin{
        placeId  ' 11111 '
    }
    destination{
        origin 'asas ' 
    }
    mode Mode.DRIVING
    alternatives true
    avoid Avoid.TOLLS
    language Language.IN
    region Region.IN
    arrival_time 
    ..
    
}
```

