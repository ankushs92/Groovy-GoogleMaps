package in.ankushs.googlemaps.domain

import groovy.transform.ToString

import com.fasterxml.jackson.annotation.JsonProperty

@ToString
final class Southwest {
	final double lat
	final double lng
	
	def Southwest(@JsonProperty("lat")  double lat ,
				  @JsonProperty("lng")  double lng)
	{
		this.lat = lat;
		this.lng = lng;
	}
	
}
