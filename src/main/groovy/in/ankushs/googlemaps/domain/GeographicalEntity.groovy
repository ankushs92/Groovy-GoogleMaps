package in.ankushs.googlemaps.domain

import groovy.transform.ToString

import com.fasterxml.jackson.annotation.JsonProperty
@ToString
final class GeographicalEntity {

	final List<AddressComponent> addressComponents
	final String formattedAddress
	final Geometry geometry
	final String placeId
	final List<String> types
	final boolean partialMatch

	def GeographicalEntity(
			 @JsonProperty("address_components")  List<AddressComponent> addressComponents,
			 @JsonProperty("formatted_address")  String formattedAddress,
			 @JsonProperty("geometry")  Geometry geometry,
			 @JsonProperty("place_id")  String placeId,
			 @JsonProperty("types")  List<String> types,
			 @JsonProperty(value="partial_match",required=false)  boolean partialMatch
			)
	{
		this.addressComponents = addressComponents
		this.formattedAddress = formattedAddress
		this.geometry = geometry
		this.partialMatch = partialMatch
		this.placeId = placeId
		this.types = types
	}	
	
}

