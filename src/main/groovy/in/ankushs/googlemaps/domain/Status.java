package in.ankushs.googlemaps.domain;


import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import in.ankushs.googlemaps.deserializers.JacksonStatusDeserializer;

@JsonDeserialize(using=JacksonStatusDeserializer.class)
public enum Status {
    OK,
    ZERO_RESULTS,
    OVER_QUERY_LIMIT,
    REQUEST_DENIED,
    INVALID_REQUEST,
    UNKNOWN_ERROR;
}

