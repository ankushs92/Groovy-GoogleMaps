package in.ankushs.googlemaps.domain

import groovy.transform.ToString

import com.fasterxml.jackson.annotation.JsonProperty

@ToString
final class Viewport {
	final Northeast northeast;
	final Southwest southwest;

	def Viewport(@JsonProperty("northeast")  Northeast northeast , @JsonProperty("southwest")  Southwest southwest)
	{
		northeast = northeast
		southwest = southwest
	}
}

