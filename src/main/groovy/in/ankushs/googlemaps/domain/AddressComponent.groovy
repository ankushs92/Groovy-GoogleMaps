package in.ankushs.googlemaps.domain

import groovy.transform.ToString

import com.fasterxml.jackson.annotation.JsonProperty

@ToString
class AddressComponent {

	final String longName
	final String shortName
	final List<AddressComponentType> types
	
	def AddressComponent(@JsonProperty("long_name")  String longName,
						 @JsonProperty("short_name")  String shortName,
						 @JsonProperty("types")  List<AddressComponentType> types)
	{
		longName = longName 
		shortName = shortName
		types = types
	}
}

