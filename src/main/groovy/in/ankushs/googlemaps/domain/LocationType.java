package in.ankushs.googlemaps.domain;


import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import in.ankushs.googlemaps.deserializers.JacksonLocationTypeDeserializer;

@JsonDeserialize(using=JacksonLocationTypeDeserializer.class)
public enum LocationType {
    ROOFTOP,
    RANGE_INTERPOLATED,
    GEOMETRIC_CENTER,
    APPROXIMATE
}


