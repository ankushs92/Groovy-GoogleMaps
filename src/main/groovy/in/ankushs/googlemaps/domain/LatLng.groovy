package in.ankushs.googlemaps.domain

import groovy.transform.ToString

import com.fasterxml.jackson.annotation.JsonProperty

@ToString
final class LatLng {
	final double lat
	final double lng
	
	def LatLng(@JsonProperty("lat")  double lat,@JsonProperty("lng")  double lng)
	{
		lat = lat
		lng = lng
	}
}

