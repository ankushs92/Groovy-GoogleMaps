package in.ankushs.googlemaps.domain

import com.fasterxml.jackson.annotation.JsonProperty

 final class Geometry {
	
	 final Bounds bounds
	 final LatLng latLng;
	 final Viewport viewport
	 final LocationType locationType;

	
	def Geometry(
			@JsonProperty("bounds")  Bounds bounds,
			@JsonProperty("location_type")  LocationType locationType,
			@JsonProperty("location")  LatLng latLng,
			@JsonProperty("viewport")  Viewport viewport
		)
	{
		this.bounds = bounds;
		this.latLng = latLng;
		this.viewport = viewport;
		this.locationType = locationType;
	}

}

