package in.ankushs.googlemaps.domain;

public enum Language {
    AR,
    BG,
    BN,
    CA,
    CS,
    DA,
    DE,
    EL,
    EN,
    EN_AU,
    EN_GB,
    ES,
    EU,
    FA,
    FI,
    FIL,
    FR,
    GL,
    GU,
    HI,
    HR,
    HU,
    ID,
    IT,
    IW,
    JA,
    KN,
    KO,
    LT,
    LV,
    ML,
    MR,
    NL,
    NO,
    PL,
    PT,
    PT_BR,
    PT_PT,
    RO,
    RU,
    SK,
    SL,
    SR,
    SV,
    TA,
    TE,
    TH,
    TL,
    TR,
    UK,
    VI,
    ZH_CN,
    ZH_TW;
}
