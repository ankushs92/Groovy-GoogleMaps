package in.ankushs.googlemaps.domain

import groovy.transform.ToString

import com.fasterxml.jackson.annotation.JsonProperty
@ToString
final class Bounds {
	final Northeast northeast
	final Southwest southwest
	
	def Bounds(@JsonProperty("northeast")  Northeast northeast,
			   @JsonProperty("southwest")  Southwest southwest)
	{
		northeast = northeast
		southwest = southwest
	}
}

