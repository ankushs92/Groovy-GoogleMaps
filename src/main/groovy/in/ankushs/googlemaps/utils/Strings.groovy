package in.ankushs.googlemaps.utils

/**
 * Created by Ankush on 23/05/16.
 */
/**
 *
 * String utilities.
 * @author Ankush Sharma
 */
 class Strings {

    private Strings(){}

    /**
     * Checks whether a String has any  text in it.
     * @param str the string being inspected for text
     * @return false if {@code str} is null or empty(after trimming),and true otherwise
     */
     static boolean hasText(final String str){
        if(str==null){
            return false;
        }
        return str.trim().length()>0;
    }

}

