package in.ankushs.googlemaps.utils

/**
 * Created by Ankush on 23/05/16.
 */
class PreConditions {

    /**
     * Ensures the string passed to the method is not empty .
     * The string will be considered empty if it is null or has no text after being trimmed.
     *
     * @param str The string against which the validation will be performed
     * @param errorMsg The message to throw in case {@code str} turns out to have no text.
     * @throws IllegalArgumentException if {@code str} has no text
     */
     static void checkEmptyString( String str ,  String errorMsg){
        if(!Strings.hasText(str)){
            throw new IllegalArgumentException(errorMsg);
        }
    }
}
