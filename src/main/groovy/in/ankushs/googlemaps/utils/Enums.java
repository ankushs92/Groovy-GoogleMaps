package in.ankushs.googlemaps.utils;

public class Enums {
	
	public static <T extends Enum<T>> T getEnumFromString(final Class<T> enumClass,final String value) {
		    if(enumClass == null){
		       throw new IllegalArgumentException("enumClass cannot be null");
		    }
		
        for (final Enum<?> enumValue : enumClass.getEnumConstants()) {
            if (enumValue.toString().equalsIgnoreCase(value)) {
                return (T) enumValue;
            }
        }

        //Construct an error message that indicates all possible values for the enum.
        final StringBuilder errorMessage = new StringBuilder();
        boolean bFirstTime = true;
        for (final Enum<?> enumValue : enumClass.getEnumConstants()) {
            errorMessage.append(bFirstTime ? "" : ", ").append(enumValue);
            bFirstTime = false;
        }
        throw new IllegalArgumentException(value + " is invalid value. Supported values are " + errorMessage);
    }
}
