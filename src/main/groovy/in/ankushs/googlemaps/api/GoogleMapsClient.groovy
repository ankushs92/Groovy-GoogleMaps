package in.ankushs.googlemaps.api

import groovy.transform.ToString
import okhttp3.OkHttpClient

import in.ankushs.googlemaps.utils.PreConditions;
/**
 * Created by Ankush on 23/05/16.
 */
@ToString
final class GoogleMapsClient {

    final String apiKey
    OkHttpClient okHttpClient

    def GoogleMapsClient(String apiKey){
        PreConditions.checkEmptyString(apiKey,"apiKey cannot be null or empty")
        this.apiKey = apiKey
    }

    public static void main(String[] args) {
        def client = new GoogleMapsClient("aaaaa")
        print client
        

    }

}
