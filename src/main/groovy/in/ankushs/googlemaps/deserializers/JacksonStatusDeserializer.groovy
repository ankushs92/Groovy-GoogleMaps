package in.ankushs.googlemaps.deserializers


import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.core.JsonProcessingException
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.JsonDeserializer
import in.ankushs.googlemaps.domain.Status
import in.ankushs.googlemaps.utils.Enums

final class JacksonStatusDeserializer extends JsonDeserializer<Status> {
	
	@Override
	 Status deserialize(JsonParser jsonParser, DeserializationContext ctxt) throws IOException, JsonProcessingException
	{
		//We can assume that the Google maps web service will always send some status , and not a blank or empty String.
		def text = jsonParser.getText().toUpperCase();
	    Status status = Enums.getEnumFromString(text, Status.class);
		status
	}
}

