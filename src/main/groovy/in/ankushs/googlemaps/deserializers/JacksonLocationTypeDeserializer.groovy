package in.ankushs.googlemaps.deserializers

import in.ankushs.googlemaps.utils.Enums

import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.core.JsonProcessingException
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.JsonDeserializer
import in.ankushs.googlemaps.domain.LocationType

final class JacksonLocationTypeDeserializer extends JsonDeserializer<LocationType> {

	@Override
	 LocationType deserialize( JsonParser jsonParser, DeserializationContext ctxt)
			throws IOException, JsonProcessingException 
	{
		def text = jsonParser.getText().toUpperCase();
		LocationType locationType = Enums.getEnumFromString(text, LocationType.class);
	    locationType
	}

}

