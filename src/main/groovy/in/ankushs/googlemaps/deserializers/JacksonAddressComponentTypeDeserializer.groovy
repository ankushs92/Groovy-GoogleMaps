package in.ankushs.googlemaps.deserializers

import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.core.JsonProcessingException
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.JsonDeserializer
import in.ankushs.googlemaps.domain.AddressComponentType
import in.ankushs.googlemaps.utils.Enums
/**
 * Created by Ankush on 23/05/16.
 */
 final class JacksonAddressComponentTypeDeserializer extends JsonDeserializer<AddressComponentType> {

    @Override
     AddressComponentType deserialize( JsonParser jsonParser,  DeserializationContext ctxt)
            throws IOException, JsonProcessingException
    {
        def text = jsonParser.text.toUpperCase()
        AddressComponentType addressComponentType = Enums.getEnumFromString(AddressComponentType.class,text)
        addressComponentType
    }
}


